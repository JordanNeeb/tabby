import Vuex from "vuex";
import Vue from "vue";

import news from "./modules/news";
import cats from "./modules/cats";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    news,
    cats
  }
});
