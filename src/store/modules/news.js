import axios from "axios";
import moment from "moment";
import queryString from "query-string";

import { uris } from "../../config";
import { comparePoints } from "../../utils";

const state = {
  articles: [],
  newsErrors: []
};

const getters = {
  articles: state => state.articles,
  article: state =>
    (state.article = state.articles.find(
      ({ id }) => id === parseInt(queryString.parse(window.location.search).id)
    )),
  prettyJSON: (state, getters) => JSON.stringify(getters.article, null, 4),
  newsErrors: state => state.newsErrors
};

const actions = {
  fetchNews({ commit }) {
    axios
      .get(uris.HNPWA_API)
      .then(res => {
        // Sort by points in descending order
        const sorted = res.data.sort(comparePoints);

        // Slice before formatting
        const top25 = sorted.slice(0, 25);

        // Add formatted date
        for (let i = 0; i < top25.length; i++) {
          top25[i].date = moment.unix(top25[i].time).format("DD/MM/YYYY");
        }

        commit("updateArticles", top25);
      })
      .catch(err => {
        commit("updateNewsErrors", err.response);
      });
  }
};

const mutations = {
  updateArticles: (state, articles) => (state.articles = articles),
  updateNewsErrors: (state, newsErrors) => (state.newsErrors = newsErrors)
};

export default {
  state,
  getters,
  actions,
  mutations
};
