import axios from "axios";

import { uris } from "../../config";

const state = {
  catImageURL: "",
  catsErrors: []
};

const getters = {
  catImageURL: state => state.catImageURL,
  catsErrors: state => state.catsErrors
};

const actions = {
  fetchCatImage({ commit }) {
    axios
      .get(uris.AWS_CAT_API)
      .then(res => {
        commit("updateCatImageURL", res.data.file);
      })
      .catch(err => {
        commit("updateErrors", err.response);
      });
  }
};

const mutations = {
  updateCatImageURL: (state, catImageURL) => (state.catImageURL = catImageURL),
  updateCatsErrors: (state, catsErrors) => (state.catsErrors = catsErrors)
};

export default {
  state,
  getters,
  actions,
  mutations
};
