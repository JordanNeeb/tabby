import Vue from "vue";
import Router from "vue-router";
import Home from "./pages/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/news",
      name: "news",
      component: () => import("./pages/News.vue")
    },
    {
      path: "/news?id=:id",
      name: "article",
      component: () => import("./pages/Article.vue")
    },
    {
      path: "/cats",
      name: "cats",
      component: () => import("./pages/Cats.vue")
    }
  ]
});
