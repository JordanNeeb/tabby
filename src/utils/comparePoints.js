function comparePoints(a, b) {
  let comparison = 0;

  if (a.points > b.points) {
    comparison = 1;
  } else if (a.points < b.points) {
    comparison = -1;
  }

  return comparison * -1;
}

export default comparePoints;
